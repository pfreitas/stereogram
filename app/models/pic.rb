
class Pic < ActiveRecord::Base

  belongs_to :stereo

  has_attached_file :file, styles: { medium: "640x640#", thumb: "100x100#" }
  validates_attachment_content_type :file, :content_type => ["image/jpg", "image/jpeg"]

  after_save :process_stereo

  protected

  def process_stereo
    # we got all the pics, we can generate the gif now
    if stereo.pics.count == stereo.pic_count
      stereo.process!
    end
  end
end
