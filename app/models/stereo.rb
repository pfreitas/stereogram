class Stereo < ActiveRecord::Base
  include Magick

  acts_as_taggable_on :tags

  has_many :pics

  validates :pic_count, presence: true
  validates :uuid, presence: true

  before_save :generate_uuid, on: :create
  before_save :set_effect, on: :create

  def process!
    if is_3d? && pic_count == 2
      generate_3d
    else
      generate_gif
    end
    # imagga tagging
    Imagga.tag(self)
  end

  def is_gif?
    effect.nil? || effect == 1
  end

  def is_3d?
    effect == 2
  end

  def path(format='gif')
    "public/stereos/#{uuid}.#{format}"
  end

  protected

  def set_effect
    self.effect ||= 1
  end

  def generate_uuid
    self.uuid ||= SecureRandom.uuid
  end

  def generate_gif
    gif = ImageList.new
    pics.each do |pic|
      gif << Magick::Image::read(pic.file.path(:medium)).first
    end
    gif.write(path)
  end

  def generate_3d
    left  = Magick::ImageList.new(pics.first.file.path(:medium))
    right = Magick::ImageList.new(pics.second.file.path(:medium))
    st = left.stereo(right)
    st.write(path('jpg'))
  end
end
