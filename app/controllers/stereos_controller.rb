class StereosController < ApplicationController

  def create
    @stereo = Stereo.new(stereo_params)
    @stereo.save!
    render json: @stereo
  end

  def update
    @stereo = Stereo.find(params[:id])
    @stereo.process!
    render json: @stereo
  end

  def index
    if params[:tag]
      @stereos = Stereo.tagged_with(params[:tag])
    else
      @stereos = Stereo.order('id desc')
    end
    respond_to do |format|
      format.json { render json: @stereos.limit(10).to_json }
      format.html { @stereos = @stereos.limit(180) }
    end
  end

  def show
    @stereo = Stereo.find(params[:id])
    respond_to do |format|
      format.json { render json: @stereo.to_json }
      format.html
    end
  end

  protected

  def stereo_params
    params.require(:stereo).permit(:uuid, :title)
  end
end
