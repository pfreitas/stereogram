class PicsController < ApplicationController
  def new
    @pic = Pic.new
  end

  def create
    @pic = Pic.new(pic_params)
    @pic.stereo = Stereo.where(uuid: params[:uuid], pic_count: params[:pic_count]).first_or_create
    @pic.save!
    render json: @pic
  end

  protected

  def pic_params
    params.require(:pic).permit(:file)
  end
end
