class CreatePics < ActiveRecord::Migration
  def change
    create_table :pics do |t|
      t.integer :stereo_id
      t.timestamps
    end
  end
end
