class CreateStereos < ActiveRecord::Migration
  def change
    create_table :stereos do |t|
      t.string :uuid
      t.string :uri
      t.string :title
      t.timestamps
    end
  end
end
