class AddFileFields < ActiveRecord::Migration
  def up
    add_attachment :pics, :file
  end

  def down
    remove_attachment :pics, :file
  end
end
