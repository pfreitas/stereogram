class AddEffectToStereos < ActiveRecord::Migration
  def change
    add_column :stereos, :effect, :integer
  end
end
