Rails.application.routes.draw do

  resources :pics

  resources :stereos do
    resources :pics
  end

  root to: 'stereos#index'

end
