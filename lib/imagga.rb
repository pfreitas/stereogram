require "net/http"
require "uri"

class Imagga
  def self.tag(stereo)
    key = 'acc_0023f1019d16cd8'
    secret = '5a04ec728f53747547f28c0a063bd2e9'

    pic_url = 'http://stereogram.indios.rip/' + stereo.pics.first.file.url
    call_url = "http://api.imagga.com/v1/tagging?url=#{pic_url}"

    Rails.logger.debug "IMAGGA: Calling #{call_url}"

    uri = URI.parse(call_url)

    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    request.basic_auth(key, secret)
    response = http.request(request)
    j = JSON.parse(response.body)

    tags = j['results'].first['tags']

    Rails.logger.debug "IMAGGA: #{tags.inspect}"

    tags[0,3].each do |tag|
      Rails.logger.debug "IMAGGA: tagging stereo with #{tag['tag']}"
      stereo.tag_list.add tag['tag']
    end

    stereo.save
  end
end
